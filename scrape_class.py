import subprocess
import uuid
import pandas as pd
from codetiming import Timer
import sys
import os
from bs4 import BeautifulSoup
import time
import shutil
import tldextract
from selenium import webdriver
import requests
import urllib3
import urllib
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def open_selenium(url, browser_type="Chrome", wait_time=500, headless=True):
    if headless:
        chromeOptions = webdriver.ChromeOptions()
        chromeOptions.add_argument("--headless")
        chromeOptions.add_argument("--remote-debugging-port=9222")
        chromeOptions.add_argument('--no-sandbox')
        browser = webdriver.Chrome('./chromedriver',options=chromeOptions)
    elif browser_type == "Chrome":
        browser = webdriver.Chrome('./chromedriver')
    else:
        browser = webdriver.Firefox('./geckodriver')
    browser.get(url)
    browser.implicitly_wait(wait_time)
    return browser

def close_selenium(browser):
    browser.close()

def extract_links_and_images(url, page):
    site = tldextract.extract(url).fqdn
    href_internal_tags = []
    img_internal_tags = []
    href_external_tags = []
    img_external_tags = []
    if page is None:
        print("Can't read page")
    else:
        # soup = BeautifulSoup(page, 'html.parser')
        # lxml is better in accuracy
        soup = BeautifulSoup(page, 'lxml')
        ahrefs = soup.findAll('a')
        for link in ahrefs:
            if 'href' in link.attrs:
                href_link = link['href']
            elif 'data-src' in link.attrs:
                href_link = link['data-src']
            else:
                href_link = ''
            if href_link != '':
                href_link_site = tldextract.extract(href_link).fqdn
                if href_link_site.strip() == '':
                    #This means no domain in the link
                    new_link = site+href_link
                    if not new_link in href_internal_tags  and not href_link.startswith('#'):
                        href_internal_tags.append(new_link)
                elif href_link_site == site:
                    #This means the same site
                    if not href_link in href_internal_tags:
                        href_internal_tags.append(href_link)
                else:
                    if not href_link in href_external_tags and not href_link.startswith('#'):
                        href_external_tags.append(href_link)
        iframes = soup.findAll(['iframe','frame'])
        for frame in iframes:
            if 'src' in frame.attrs:
                iframe_src = frame['src']
                href_external_tags.append(iframe_src)
            else:
                continue
        imghrefs1 = soup.findAll('img')
        imghrefs2 = soup.findAll('canvas')
        imghrefs = imghrefs1 + imghrefs2
        print("Here are all the images", imghrefs1, imghrefs2)
        for img in imghrefs:
            if 'src' in img.attrs:
                # test external
                img_link = img['src']
            elif 'data-src' in img.attrs:
                img_link = img['data-src']
            else:
                img_link = ''
            if img_link != '':
                img_link_site = tldextract.extract(img_link).fqdn
                if img_link_site.strip() == '':
                    new_link = site + "/" + img_link
                    # This means no domain in the link
                    if not new_link in img_internal_tags  and not img_link.startswith('data:image') and not 'maps.' in img_link:
                        img_internal_tags.append(new_link)
                elif img_link_site == site:
                    if not img_link in img_internal_tags  and not img_link.startswith('data:image') and not 'maps.' in img_link:
                        # This means the same site
                        img_internal_tags.append(img_link)
                else:
                    if not img_link in img_external_tags  and not img_link.startswith('data:image') and not 'maps.' in img_link:
                        img_external_tags.append(img_link)
    #print("Here are the image internals", img_internal_tags)
    #print("Here are the href internals", href_internal_tags)
    #print("Here are the image externals", img_external_tags)
    #print("Here are the href externals", href_external_tags)
    return img_internal_tags, img_external_tags, href_internal_tags, href_external_tags

def check_sentence_ok(text):
# Wrapper for future use
    return True

def is_external(link, site):
    if len(site)==0:
       return True
    else:
       if not link.startswith('http'):
          return False
       else:
          linksite = tldextract.extract(link).fqdn
          if linksite.strip() == site.strip():
             return False
          else:
             return True

def check_file_extension(external):
    if '.jpg' in external or '.gif' in external or '.jpeg' in external or '.webp' in external or '.png' in external or '.bmp' in external:
        return 'image'
    if '.avi' in external or '.mov' in external or '.mp4' in external:
        return 'video'
    if '.jsp' in external or '.htm' in external or '.php' in external or '.asp' in external:
        return 'html'
    if '.js' in external or '.css' in external or '.svg' in external or '.otf' in external or '.tff' in external or '.ttf' in external or '.woff' in external or '.eot' in external or '.pdf' in external or '.zip' in external or '.txt' in external:
        return 'skip'
    elif '.cer' in external or '.cfm' in external or '.csr' in external or '.dcr' in external or '.rss' in external or '.ai' in external or '.eps' in external:
        return 'skip'
    elif '.log' in external:
        return 'skip'
    elif '.mp3' in external or '.wav' in external:
        return 'audio'
    else:
        return 'html'

def extract_sentences_from_file(filename,url):
    site = tldextract.extract(url).fqdn
    sentences = []
    page = open(filename, "r", encoding="utf-8", errors='ignore')
    hreftags = []
    imgtags = []
    internal_hreftags = []
    internal_imgtags = []
    if page is None:
        print("Can't read url")
    else:
        #soup = BeautifulSoup(page, 'html.parser')
        #lxml is better in accuracy
        soup = BeautifulSoup(page, 'lxml')
        html = soup.findAll('html')
        if len(html)==0:
           split = ''
        else:
           split = soup.text
        ahrefs = soup.findAll('a')
        for link in ahrefs:
            if 'href' in link.attrs:
               #test external
               if is_external(link['href'], site):
                  hreftags.append(link['href'])
               else:
                   internal_hreftags.append(link['href'])
            if 'data-src' in link.attrs:
               #test external
               if is_external(link['data-src'], site):
                  hreftags.append(link['data-src'])
               else:
                   internal_hreftags.append(link['data-src'])
        iframes = soup.findAll(['iframe','frame'])
        for frame in iframes:
            if 'src' in frame.attrs:
                iframe_src = frame['src']
                hreftags.append(iframe_src)
            else:
                continue
        imghrefs = soup.findAll('img')
        for img in imghrefs:
            if 'src' in img.attrs:
               #test external
               if is_external(img['src'], site):
                    imgtags.append(img['src'])
               else:
                    internal_imgtags.append(img['src'])
            if 'data-src' in img.attrs:
               #test external
               if is_external(img['data-src'], site):
                    imgtags.append(img['data-src'])
               else:
                    internal_imgtags.append(img['data-src'])
        imghrefs = soup.findAll('canvas')
        for img in imghrefs:
            if 'src' in img.attrs:
               #test external
               if is_external(img['src'], site):
                  imgtags.append(img['src'])
               else:
                   internal_imgtags.append(img['src'])
            if 'data-src' in img.attrs:
               #test external
               if is_external(img['data-src'], site):
                  imgtags.append(img['data-src'])
               else:
                   internal_imgtags.append(img['data-src'])
        background_images = get_background_images(soup)
        for image in background_images:
            if is_external(image, site):
                imgtags.append(image)
            else:
                internal_imgtags.append(image)
        splits = split.split('\n')
        for x in splits:
            if check_sentence_ok(x):
                sentences.append(x.replace('\t',''))
    sentence_dict = []
    for line in sentences:
        if len(line)>0:
            sentence_dict.append({'filename':filename, 'text':line})
    external_images_dict = []
    for link in imgtags:
        external_images_dict.append({'filename':filename, 'image':link})
    external_href_dict = []
    for link in hreftags:
        external_href_dict.append({'filename':filename, 'href':link})
    internal_href_dict = []
    for link in internal_hreftags:
        internal_href_dict.append({'filename':filename, 'href':link})
    internal_images_dict = []
    for link in internal_imgtags:
        internal_images_dict.append({'filename':filename, 'image':link})
    return  internal_images_dict, internal_href_dict, external_images_dict, external_href_dict, sentence_dict


def get_background_images(soup):
    tags=soup.findAll()
    background_images=[]
    for tag in tags:
        tag_str=str(tag)
        if 'background-image: url(' in tag_str:
            start_index = tag_str.find('background-image: url(') + 22
            end_index = tag_str.find(')', start_index)
            background_url=tag_str[start_index:end_index].strip('"')
            background_images.append(background_url)
        if 'data-src' in tag.attrs:
            data_src=tag['data-src']
            data_type=check_file_extension(data_src)
            if data_type=='image':
                background_images.append(data_src)
    background_images=list(set(background_images))
    return background_images


def extract_assets_from_page_loaded(page):
    sentences = []
    hreftags = []
    imgtags = []
    if page is None:
        print("Can't read page")
    else:
        #soup = BeautifulSoup(page, 'html.parser')
        #lxml is better in accuracy
        soup = BeautifulSoup(page, 'lxml')
        html = soup.findAll('html')
        if len(html)==0:
           split = ''
        else:
           split = soup.text
        ahrefs = soup.findAll('a')
        for link in ahrefs:
            if 'href' in link.attrs:
                hreftags.append(link['href'])
            if 'data-src' in link.attrs:
                hreftags.append(link['data-src'])
        iframes = soup.findAll(['iframe','frame'])
        for frame in iframes:
            if 'src' in frame.attrs:
                iframe_src = frame['src']
                hreftags.append(iframe_src)
            else:
                continue
        imghrefs = soup.findAll('img')
        for img in imghrefs:
            if 'src' in img.attrs:
                imgtags.append(img['src'])
            if 'data-src' in img.attrs:
                imgtags.append(img['data-src'])
        imghrefs = soup.findAll('canvas')
        for img in imghrefs:
            if 'src' in img.attrs:
                imgtags.append(img['src'])
            if 'data-src' in img.attrs:
                imgtags.append(img['data-src'])
        splits = split.split('\n')
        for x in splits:
            if check_sentence_ok(x):
                sentences.append(x.replace('\t',''))
    sentence_dict = []
    for line in sentences:
        if len(line)>0:
            sentence_dict.append(line)
    return imgtags, hreftags, sentence_dict

class Site:
    download_log_results = []
    download_levels = 5
    download_images = True
    download_videos = True
    download_texts = True
    download_audios = True
    download_method = 'wget2'
    download_exclude_files = []
    download_output_directory = "."
    download_errorcode = 0
    download_tries = 1
    download_cookies = False
    download_parent = False
    download_waitretry = 1
    download_recursive = True
    download_robots = False
    download_no_check_certificate = True
    spider_tries = 5
    download_logfile = True
    logfile_name = './wget.log'
    download_read_timeout = 5
    download_timeout = 10
    download_page_requisites = False
    download_no_header_structure = True
    mock = False
    total_image_files = 0
    total_video_files = 0
    total_html_files = 0
    total_sentences = []
    image_urls = []
    html_urls = []
    video_urls = []
    sentences_extracted = []
    external_image_urls = []
    external_href_urls = []
    site_header = None
    site_server_header = None
    full_site_address = None

    def __init__(self, url, type='site'):
        self.url = url
        self.type = type

    def get(self):
        _dict = {"levels": self.download_levels, "download_images": self.download_images,
                 "download_videos": self.download_videos, "download_texts": self.download_texts,
                 "download_audios": self.download_audios, "download_method": self.download_method,
                 "download_exclude_files": self.download_exclude_files, "download_output_directory": self.download_output_directory,
                 "download_errorcode": self.download_errorcode, "download_tries": self.download_tries,
                 "download_cookies": self.download_cookies, "download_parent": self.download_parent,
                 "download_waitretry": self.download_waitretry, "download_recursive": self.download_recursive,
                 "download_robots": self.download_robots, "download_no_check_certificate": self.download_no_check_certificate,
                 "spider_tries": self.spider_tries,"download_logfile": self.download_logfile,
                 "logfile_name": self.logfile_name,"download_read_timeout": self.download_read_timeout,
                 "download_no_header_structure": self.download_no_header_structure,
                 "download_timeout": self.download_timeout, "download_page_requisites": self.download_page_requisites,
                 "mock": self.mock, "url": self.url,"type":self.type,
                 "total_image_files": self.total_image_files, "total_video_files": self.total_video_files,
                 "total_html_files": self.total_html_files, "image_urls": self.image_urls,
                 "html_urls": self.html_urls, "video_urls": self.video_urls,
                 "sentences_extracted": self.sentences_extracted, "external_image_urls": self.external_image_urls,
                 "external_href_urls": self.external_href_urls, "total_sentences": self.total_sentences,
                 "download_log_results": self.download_log_results, "full_site_address": self.full_site_address,
                 "site_header": self.site_header, "site_server_header": self.site_server_header
        }
        return _dict

    def set(self, fields_dict):
        if 'levels' in fields_dict:
            self.download_levels = fields_dict['levels']
        if 'download_images' in fields_dict:
            self.download_images = fields_dict['download_images']
        if 'download_videos' in fields_dict:
            self.download_videos = fields_dict['download_videos']
        if 'download_texts' in fields_dict:
            self.download_texts = fields_dict['download_texts']
        if 'download_audios' in fields_dict:
            self.download_audios = fields_dict['download_audios']
        if 'download_method' in fields_dict:
            self.download_method = fields_dict['download_method']
        if 'download_exclude_files' in fields_dict:
            self.download_exclude_files = fields_dict['download_exclude_files']
        if 'download_output_directory' in fields_dict:
            self.download_output_directory = fields_dict['download_output_directory']
        if 'download_errorcode' in fields_dict:
            self.download_errorcode = fields_dict['download_errorcode']
        if 'download_tries' in fields_dict:
            self.download_tries = fields_dict['download_tries']
        if 'download_cookies' in fields_dict:
            self.download_cookies = fields_dict['download_cookies']
        if 'download_parent' in fields_dict:
            self.download_parent = fields_dict['download_parent']
        if 'download_waitretry' in fields_dict:
            self.download_waitretry = fields_dict['download_waitretry']
        if 'download_recursive' in fields_dict:
            self.download_recursive = fields_dict['download_recursive']
        if 'download_robots' in fields_dict:
            self.download_robots = fields_dict['download_robots']
        if 'download_logfile' in fields_dict:
            self.download_logfile = fields_dict['download_logfile']
        if 'logfile_name' in fields_dict:
            self.logfile_name = fields_dict['logfile_name']
        if 'download_read_timeout' in fields_dict:
            self.download_read_timeout = fields_dict['download_read_timeout']
        if 'download_no_header_structure' in fields_dict:
            self.download_no_header_structure = fields_dict['download_no_header_structure']
        if 'download_page_requisites' in fields_dict:
            self.download_page_requisites = fields_dict['download_page_requisites']
        if 'download_timeout' in fields_dict:
            self.download_timeout = fields_dict['download_timeout']
        if 'spider_tries' in fields_dict:
            self.spider_tries = fields_dict['spider_tries']
        if 'mock' in fields_dict:
            self.mock = fields_dict['mock']
        if 'type' in fields_dict:
            self.type = fields_dict['type']
        if 'full_site_address' in fields_dict:
            self.full_site_address = fields_dict['full_site_address']


    def check_for_redirect(self, html):
        soup = BeautifulSoup(html, 'lxml')
        redirect = soup.select('meta[http-equiv="refresh" i]')
        if len(redirect) < 1:
            return 0, 'no redirect'
        redirect_address = redirect[0]['content']
        index = redirect_address.lower().find('url=')
        if index < 0:
            return 0, 'no redirect'
        url = redirect_address[index + 4:]
        return 1, url

    def site_recommendation(self, page):
        if self.site_server_header == 'nginx':
            return 'selenium'
        elif '<%' in page or '%>' in page:
            return 'selenium'
        elif 'data-react' in page:
            return 'selenium'
        elif 'wp-content' in page:
            return 'wget2'
        else:
            imgtags, hreftags, sentence_dict  = extract_assets_from_page_loaded(page)
            if len(imgtags) < 2 and len(hreftags) < 3:
                return 'selenium'
            elif len(sentence_dict) <10:
                return 'selenium'
            else:
                return 'wget2'

    def site_investigation(self, save_page_results=False):
        #print("original url", self.url)
        url = self.url
        Done = False
        while not Done:
            if not url.startswith('http'):
                url = url + 'http://'
            error = False
            try:
                #print("Getting...", url)
                r = requests.get(url, verify=False,timeout=(20,5))
                #print(r.status_code)
            except ConnectionError:
                #print("Connection error")
                error = True
                self.download_errorcode = 700
                self.download_method = 'error ' + str(self.download_errorcode)
                self.full_site_address = self.url
            except Exception as e:
                error = True
                #print("Error: ", e, 'on ', url)
                self.download_errorcode = 800
                self.download_method = 'error '+str(self.download_errorcode)
                self.full_site_address = self.url
            if not error:
                self.download_errorcode = r.status_code
                self.full_site_address = r.url
                self.site_server_header =r.headers.get('Server')
                self.site_header = r.headers
                if self.download_errorcode == 200:
                    redirected, new_url = self.check_for_redirect(r.text)
                    if redirected:
                        #print("Redirected...")
                        if new_url.startswith('/'):
                            url = r.url + new_url
                        elif new_url.startswith('http'):
                            url = new_url
                        else:
                            url = url + '/' + new_url
                    else:
                        Done = True
                        # What is the correct method?
                        self.download_method = self.site_recommendation(r.text)
                elif self.download_errorcode <= 499:
                    Done = True
                    self.download_method = 'unauthorized_'+str(self.download_errorcode)
                elif self.download_errorcode <= 599:
                    Done = True
                    self.download_method = 'selenium'
                else:
                    Done = True
                    self.download_method = 'unknown_errorcode_'+str(self.download_errorcode)
            else:
                Done = True
                self.download_method = 'unknown_errorcode_'+str(self.download_errorcode)
        if save_page_results:
            # print("Saving the results from the investigation...")
            target_directory = self.download_output_directory
            if target_directory.endswith('/'):
                target_directory = target_directory[:-1]
            try:
                with open(target_directory + "/investigation_html.txt", "w") as f:
                    f.write(r.text)
            except Exception as e:
                print("Error:", e)
                print("Can't save investigation with the site")
        return

    def get_storage_space(self):
        prefix = '.'
        path = os.path.join(prefix, self.download_output_directory)
        total_size = 0
        for dirpath, dirnames, filenames in os.walk(path):
            for f in filenames:
                fp = os.path.join(dirpath, f)
                # skip if it is symbolic link
                if not os.path.islink(fp):
                    total_size += os.path.getsize(fp)
        total_mb = int((total_size/1024 * 100) + .5) / 100
        return total_mb

    def scrape(self, headless):
        if self.download_method=='wget2':
            self.download_by_wget2()
        elif self.download_method=='httrack':
            self.download_by_httrack()
        elif self.download_method=='spider':
            self.download_by_spider()
        elif self.download_method=='selenium':
            self.download_by_selenium(headless)
        return

    def download_by_copy(self, original_directory):
        returncode = shutil.copytree(original_directory,self.download_output_directory)
        self.download_errorcode = returncode
        return

    def download_by_httrack(self):
        process_transaction = []
        #if self.download_timeout > 0:
        #    process_transaction.append('timeout')
        #    process_transaction.append(str(self.download_timeout) + 'm')
        process_transaction.append('httrack')
        process_transaction.append(self.url)
        process_transaction.append('--path=' + self.download_output_directory)
        #if self.download_logfile:
        #    process_transaction.append('--output-file=' + self.logfile_name)
        if self.download_read_timeout > 0:
            process_transaction.append('--timeout=' + str(self.download_read_timeout))
        if self.download_waitretry > 0:
            process_transaction.append('--retries=' + str(self.download_waitretry))
        process_transaction.append('--depth=' + str(self.download_levels))
        if self.mock:
            print('process_transaction: ', process_transaction)
        else:
            process = subprocess.run(process_transaction)
            self.download_errorcode = process.returncode
        return

    def download_by_wget2_single_asset(self,url):
        process_transaction = []
        process_transaction.append('wget2')
        process_transaction.append('--directory-prefix=' + self.download_output_directory)
        if self.download_read_timeout>0:
            process_transaction.append('--read-timeout='+str(self.download_read_timeout))
        if not self.download_cookies:
            process_transaction.append('--no-cookies')
        if self.download_waitretry>0:
            process_transaction.append('--waitretry='+str(self.download_waitretry))
        process_transaction.append('--tries=2')
        process_transaction.append(url)
        process = subprocess.run(process_transaction)
        return

    def download_by_splash_single_asset(self, url, port=8050):
        target_directory = self.download_output_directory
        if target_directory.endswith('/'):
            target_directory = target_directory[:-1]
        try:
            page = requests.get('http://localhost:'+str(port)+'/render.html',params={'url': url, 'wait': self.download_waitretry})
            if page.status_code == 200:
                results = page.text
            else:
                print("PROBLEM of error code", page.status_code)
                results = ""
        except Exception as e:
            print("PROBLEM OF SPLASH NOT WORKING!",e)
            results = ""
        try:
            with open(target_directory + "/selenium_html.txt", "w") as f:
                f.write(results)
        except Exception as e:
            print("Error:", e)
            print("Can't save the base page source - likely problem with site")
        return

    def download_by_selenium_single_asset(self,url, headless):
        process_transaction = []
        browser_type = "Chrome"
        target_directory = self.download_output_directory
        if target_directory.endswith('/'):
            target_directory = target_directory[:-1]
        browser = open_selenium(self.url, browser_type, self.download_waitretry, headless)
        time.sleep(self.download_read_timeout)
        try:
            with open(target_directory + "/selenium_html.txt", "w") as f:
                f.write(browser.page_source)
        except Exception as e:
            print("Error:", e)
            print("Can't save the base page source - likely problem with site")
        close_selenium(browser)
        return

    def download_by_selenium(self, headless):
        process_transaction = []
        browser_type = "Chrome"
        url = self.url
        target_directory = self.download_output_directory
        if target_directory.endswith('/'):
            target_directory = target_directory[:-1]
        browser = open_selenium(self.url, browser_type, self.download_waitretry, headless)
        time.sleep(self.download_read_timeout)
        try:
            with open(target_directory+"/index.html","w") as f:
                f.write(browser.page_source)
        except Exception as e:
            print("Error:", e)
            print("Can't save the base page source - likely problem with site")
        img_internal_tags, img_external_tags, href_internal_tags, href_external_tags = extract_links_and_images(self.url, browser.page_source)
        for image in img_external_tags:
            image_sanitized = image
            #image_sanitized = image.replace('//', '/')
            print("Attempting download of image: ", image_sanitized)
            self.download_by_wget2_single_asset(image_sanitized)
        for image in img_internal_tags:
            #image_sanitized = image.replace('//', '/')
            image_sanitized = image
            print("Attempting download of image: ", image_sanitized)
            self.download_by_wget2_single_asset(image_sanitized)
        count = 2
        for href in href_internal_tags:
            href_sanitized = href.replace('//', '/')
            if not href_sanitized.startswith('http'):
                href_sanitized = 'http://'+href_sanitized
            try:
                if not href_sanitized.startswith('#') and not 'mailto:' in href_sanitized:
                    print("Getting next href: ", href_sanitized)
                    browser.get(href_sanitized)
                    browser.implicitly_wait(self.download_waitretry)
                    time.sleep(self.download_read_timeout)
                    with open(target_directory+"/index"+str(count)+".html", "w") as f:
                        f.write(browser.page_source)
                    img_internal_tags, img_external_tags, href_internal_tags, href_external_tags = extract_links_and_images(
                        self.url, browser.page_source)
                    for image in img_external_tags:
                        image_sanitized = image
                        # image_sanitized = image.replace('//', '/')
                        print("Attempting download of image: ", image_sanitized)
                        self.download_by_wget2_single_asset(image_sanitized)
                    for image in img_internal_tags:
                        # image_sanitized = image.replace('//', '/')
                        image_sanitized = image
                        print("Attempting download of image: ", image_sanitized)
                        self.download_by_wget2_single_asset(image_sanitized)
            except Exception as e:
                print("Error:", e)
                print("Can't save the links page source - likely problem with site")
            count +=1
        close_selenium(browser)
        return

    def download_by_wget2(self):
        process_transaction = []
        if self.download_timeout > 0:
            process_transaction.append('timeout')
            process_transaction.append(str(self.download_timeout) + 'm')
        process_transaction.append('wget2')
        process_transaction.append('--directory-prefix=' + self.download_output_directory)
        if self.download_logfile:
            process_transaction.append('--output-file='+self.logfile_name)
        if self.download_page_requisites:
            process_transaction.append(' --page-requisites')
        if not self.download_parent:
            process_transaction.append('--no-parent')
        if self.download_no_header_structure:
            process_transaction.append('-nH')
        if not self.download_robots:
            process_transaction.append('--robots=off')
        if self.download_read_timeout>0:
            process_transaction.append('--read-timeout='+str(self.download_read_timeout))
        if not self.download_cookies:
            process_transaction.append('--no-cookies')
        if self.download_waitretry>0:
            process_transaction.append('--waitretry='+str(self.download_waitretry))
        if self.download_no_check_certificate:
            process_transaction.append('--no-check-certificate')
        if self.download_recursive:
            process_transaction.append('--recursive')
        process_transaction.append('-l='+str(self.download_levels))
        process_transaction.append(self.url)
        if self.mock:
            print('process_transaction: ', process_transaction)
        else:
            process = subprocess.run(process_transaction)
            self.download_errorcode = process.returncode
        return

    def download_by_spider(self):
        process_transaction = []
        process_transaction.append('wget2')
        process_transaction.append('--spider')
        process_transaction.append('--directory-prefix=' + self.download_output_directory)
        if self.download_logfile:
            process_transaction.append('--output-file='+self.logfile_name)
        if self.download_read_timeout>0:
            process_transaction.append('--read-timeout='+str(self.download_read_timeout))
        if not self.download_cookies:
            process_transaction.append('--no-cookies')
        if self.download_waitretry>0:
            process_transaction.append('--waitretry='+str(self.download_waitretry))
        if self.download_no_check_certificate:
            process_transaction.append('--no-check-certificate')
        process_transaction.append(self.url)
        if self.mock:
            print('process_transaction: ', process_transaction)
        else:
            process = subprocess.run(process_transaction)
            self.download_errorcode = process.returncode
        self.download_log_results = []
        self.download_errorcode = 0
        self.full_site_address = self.url
        if self.download_logfile:
            try:
                file = open(self.logfile_name, "r", encoding="utf-8", errors='ignore')
                log_results = file.readlines()
                self.download_log_results = log_results
                last_line = str(log_results[len(log_results)-1]).strip()
                if 'HTTP response' in last_line or 'HTTP ERROR' in last_line:
                    start_final_address = last_line.find('[')
                    end_final_address = last_line.find(']')
                    if start_final_address>0:
                        self.full_site_address = last_line[start_final_address+1:end_final_address]
                    errorcode = 0
                    if 'HTTP response' in last_line:
                        try:
                            errorcode = int(last_line.split(' ')[2])
                        except:
                            errorcode = 0
                    elif 'HTTP ERROR' in last_line:
                        try:
                            errorcode = int(last_line.split(' ')[3])
                        except:
                            errorcode = 0
                    self.download_errorcode = errorcode
                elif 'Failed' in last_line or 'Checking' in last_line:
                    errorcode = 700
                    self.download_errorcode = errorcode
                else:
                    errorcode = 800
                    self.download_errorcode = errorcode
            except:
                print("No log results - possible error")
        return

    def calculate_totals(self):
        image_urls = []
        video_urls = []
        html_urls = []
        sentences_extracted = []
        external_href_urls = []
        external_images_urls = []
        total_sentences_extracted = []
        for root, dir, files in os.walk(self.download_output_directory):
            for file in files:
                ext = file.split('/')[-1].lower()
                file_type = check_file_extension(ext)
                if file_type == 'image':
                    image_urls.append({"filename": os.path.abspath(os.getcwd()) + '/' + os.path.join(root, file)})
                elif file_type == 'video':
                    video_urls.append(os.path.join(root, file))
                elif file_type == 'html':
                    filename = os.path.join(root, file)
                    internal_images_urls, internal_href_urls, new_images_urls, new_href_urls, new_sentences_extracted = extract_sentences_from_file(filename, self.url)
                    total_sentences_extracted.append(len(new_sentences_extracted))
                    sentences_extracted = sentences_extracted + new_sentences_extracted
                    external_images_urls = external_images_urls + new_images_urls
                    external_href_urls = external_href_urls + new_href_urls
                    html_urls.append(os.path.join(root, file))
        self.total_image_files = len(image_urls)
        self.total_video_files = len(video_urls)
        self.total_html_files = len(html_urls)
        self.total_sentences = total_sentences_extracted
        self.image_urls = image_urls
        self.html_urls = html_urls
        self.video_urls = video_urls
        self.sentences_extracted = sentences_extracted
        self.external_image_urls = external_images_urls
        self.external_href_urls = external_href_urls
        return

    def compare_results(self):
        url = self.full_site_address
        directory = self.download_output_directory

        i_hrefs = 0
        i_imgs = 0
        i_sent = 0
        s_hrefs = 0
        s_imgs = 0
        s_sent = 0
        w_hrefs = 0
        w_imgs = 0
        w_sent = 0

        # Check investigation results
        filename = directory+'/investigation_html.txt'
        if os.path.isfile(filename):
            internal_images_urls, internal_href_urls, new_images_urls, new_href_urls, new_sentences_extracted = extract_sentences_from_file(
                filename, url)
            i_imgs =  len(internal_images_urls) + len(new_images_urls)
            i_hrefs =  len(internal_href_urls) + len(new_href_urls)
            i_sent = len(new_sentences_extracted)
        # Check selenium results
        filename = directory+'/selenium_html.txt'
        if os.path.isfile(filename):
            internal_images_urls, internal_href_urls, new_images_urls, new_href_urls, new_sentences_extracted = extract_sentences_from_file(
                    filename, url)
            s_imgs =  len(internal_images_urls) + len(new_images_urls)
            s_hrefs =  len(internal_href_urls) + len(new_href_urls)
            s_sent = len(new_sentences_extracted)
        # Check wget2 results
        filename = directory+'/index.html'
        if os.path.isfile(filename):
            internal_images_urls, internal_href_urls, new_images_urls, new_href_urls, new_sentences_extracted = extract_sentences_from_file(
                    filename, url)
            w_imgs =  len(internal_images_urls) + len(new_images_urls)
            w_hrefs =  len(internal_href_urls) + len(new_href_urls)
            w_sent = len(new_sentences_extracted)
        results = {"investigate_images": i_imgs, "investigate_hrefs": i_hrefs, "investigate_sentences": i_sent, "selenium_images": s_imgs, "selenium_hrefs": s_hrefs, "selenium_sentences": s_sent, "wget_images": w_imgs, "wget_hrefs": w_hrefs, "wget_sentences": w_sent}
        return results

    def dl_entire_first_page(self):
        url=self.full_site_address
        directory=self.download_output_directory
        self.download_by_wget2_single_asset(url)
        self.download_by_selenium_single_asset(url,headless=True)
        res=self.compare_results()
        method=self.choose_method(res)
        if method=='selenium':
            page_path=os.path.join(directory,'selenium_html.txt')
        else:
            page_path=os.path.join(directory,'index.html')
        page=open(page_path,"r", encoding = "utf-8", errors = 'ignore')
        internal_images_dict, internal_href_dict, external_images_dict, external_href_dict, sentence_dict=extract_sentences_from_file(page_path,url)
        for img in internal_images_dict:
            img['image']=urllib.parse.urljoin(url,img['image'])
        all_images=internal_images_dict+external_images_dict
        image_dest=os.path.join(directory,'images')
        os.mkdir(image_dest)
        self.download_output_directory=image_dest
        for image in all_images:
            self.download_by_wget2_single_asset(image['image'].strip('/'))
        self.download_output_directory=directory
        return


    def choose_method(self,res):
        ratio=1.1
        if res['selenium_images']>ratio*res['wget_images'] or res['selenium_hrefs']>ratio*res['wget_hrefs'] or res['selenium_sentences']>ratio*res['wget_sentences']:
            return 'selenium'
        else:
            return 'wget2'

    def get_background_images(self,soup):
        tags=soup.findAll()
        background_images=[]
        for tag in tags:
            tag_str=str(tag)
            if 'background-image: url(' in tag_str:
                start_index = tag_str.find('background-image: url(') + 22
                end_index = tag_str.find(')', start_index)
                background_url=tag_str[start_index:end_index].strip('"')
                background_images.append(background_url)
        return background_images


def unit_case():
    urlcase = Site("http://www.google.com")
    results = urlcase.get()
    print(results)
    urlcase.mock = True
    urlcase.download_by_wget2()





def process_testcases(filename, method='wget2', remove_after=True, headless=True, save_page_results=False):
    results = []
    if filename.endswith('.csv'):
        linesin = pd.read_csv(filename)
        cases = linesin['url'].to_list()
        timer = Timer(text=f"Cases Task elapsed time: {{:.1f}}")
        timer.start()
        for case in cases:
            print("Processing: ", case, "with", method, "method")
            job_id = str(uuid.uuid1())
            os.mkdir(job_id)
            if method=='wget2':
                fields_dict = {'download_output_directory': job_id, 'levels': 2, 'logfile_name': job_id + '/wget.log', 'download_method':method}
            elif method=='httrack':
                fields_dict = {'download_output_directory': job_id, 'levels': 3, 'logfile_name': job_id + '/httrack.log', 'download_method':method}
            elif method=='spider':
                fields_dict = {'download_waitretry': 5 , 'download_read_timeout':30, 'download_output_directory': job_id, 'logfile_name': job_id + '/wget_spider.log', 'download_method':method}
            elif method=='selenium':
                fields_dict = {'download_waitretry': 500, 'download_read_timeout': 30,
                               'download_output_directory': job_id, 'logfile_name': job_id + '/selenium.log',
                               'download_method': method}
            else:
                fields_dict = {'download_output_directory': job_id, 'download_method': method}
            if case.startswith('http') or method=='spider':
                url = case
            else:
                url = 'http://'+case
            site=Site(url)
            site.set(fields_dict)
            start_time = time.time()
            print("Investigating...", site.url, "and saving ", save_page_results)
            site.site_investigation(save_page_results)
            if method == 'compare':
                #print('Comparing with a url get:')
                site.download_by_wget2_single_asset(site.full_site_address)
                site.download_by_selenium_single_asset(site.full_site_address, headless)
                #site.download_by_splash_single_asset(site.full_site_address)
                results_from_comparison = site.compare_results()
            elif method == 'dynamic':
                fields_dict = {'download_waitretry': 500, 'download_read_timeout': 30,
                               'download_output_directory': job_id, 'logfile_name': job_id + '/selenium.log',
                               'download_method': 'selenium'}
                site.set(fields_dict)
            elif method != 'check':
                site.scrape(headless)
            end_time = time.time()
            net_time = int((end_time - start_time)*100+.50)/100
            if method!='compare':
                total_mb = site.get_storage_space()
                site.calculate_totals()
                results.append({'job': job_id, 'method': site.download_method, 'url': url, 'time': net_time, 'size': total_mb, 'error': site.download_errorcode,
                            "images": site.total_image_files, "videos": site.total_video_files, "htmls": site.total_html_files,
                            "sentences": site.total_sentences, "external_images": len(site.external_image_urls),
                            "external_links": len(site.external_href_urls), "full_site_address":site.full_site_address ,"log_results":site.download_log_results,
                            "site_header": site.site_server_header})
            else:
                results_dict = {'job': job_id, 'method': site.download_method, 'url': url, 'time': net_time,  'error': site.download_errorcode,
                             "full_site_address":site.full_site_address,"site_header": site.site_server_header}
                results_dict.update(results_from_comparison)
                results.append(results_dict)
            if remove_after:
                shutil.rmtree(job_id)
        timer.stop()
    return results

def main(filename=''):
    if filename=='':
        if len(sys.argv) > 1:
            filename = sys.argv[1]
        else:
            filename = 'test.csv'
    methods = ['compare']
    remove_after = False
    headless=True
    save_page_results = True
    for method in methods:
        outputfile = filename[:-4]+'-'+method+'-results.csv'
        results = process_testcases(filename, method, remove_after, headless, save_page_results)
        results_pd = pd.DataFrame(results)
        results_pd.to_csv(outputfile, index=False)


def test_image_dl():
    #url='http://swtdz.com/'
    #url='http://nastysite.s3-website-us-east-1.amazonaws.com/girlpictures/index.html'
    #url='https://boards.4chan.org/hm/thread/2053214'
    #url='http://allhotlinks.com/'
    # url='https://daye5.cc/'
    url='https://porngull.com/'
    #url='http://chubbyloving.com/'
    #url='https://www.ebonycam.com/'
    job_id = str(uuid.uuid1())
    os.mkdir(job_id)
    fields={'download_output_directory': job_id,"full_site_address":url}
    site=Site(url)
    site.set(fields)
    site.dl_entire_first_page()

test_image_dl()
#Run the Mainline
#main("")
#main('dynamiccases.csv')
#main("dynamiccases2.csv")
#main("randomdeluxe.csv")